<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180510101528 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE conseil_municipal DROP FOREIGN KEY FK_9ACF009012469DE2');
        $this->addSql('CREATE TABLE lieux (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('DROP TABLE category');
        $this->addSql('ALTER TABLE conseil_municipal DROP FOREIGN KEY FK_9ACF009012469DE2');
        $this->addSql('ALTER TABLE conseil_municipal ADD CONSTRAINT FK_9ACF009012469DE2 FOREIGN KEY (category_id) REFERENCES lieux (id)');
        $this->addSql('ALTER TABLE event ADD lieux_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE event ADD CONSTRAINT FK_3BAE0AA7A2C806AC FOREIGN KEY (lieux_id) REFERENCES lieux (id)');
        $this->addSql('CREATE INDEX IDX_3BAE0AA7A2C806AC ON event (lieux_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE conseil_municipal DROP FOREIGN KEY FK_9ACF009012469DE2');
        $this->addSql('ALTER TABLE event DROP FOREIGN KEY FK_3BAE0AA7A2C806AC');
        $this->addSql('CREATE TABLE category (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('DROP TABLE lieux');
        $this->addSql('ALTER TABLE conseil_municipal DROP FOREIGN KEY FK_9ACF009012469DE2');
        $this->addSql('ALTER TABLE conseil_municipal ADD CONSTRAINT FK_9ACF009012469DE2 FOREIGN KEY (category_id) REFERENCES category (id)');
        $this->addSql('DROP INDEX IDX_3BAE0AA7A2C806AC ON event');
        $this->addSql('ALTER TABLE event DROP lieux_id');
    }
}

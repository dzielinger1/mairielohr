<?php

namespace App\Repository;

use App\Entity\ConseilMunicipal;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ConseilMunicipal|null find($id, $lockMode = null, $lockVersion = null)
 * @method ConseilMunicipal|null findOneBy(array $criteria, array $orderBy = null)
 * @method ConseilMunicipal[]    findAll()
 * @method ConseilMunicipal[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConseilMunicipalRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ConseilMunicipal::class);
    }

//    /**
//     * @return ConseilMunicipal[] Returns an array of ConseilMunicipal objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ConseilMunicipal
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

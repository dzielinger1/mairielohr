<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DeliberationRepository")
 * @Vich\Uploadable
 */
class Deliberation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Vich\UploadableField(mapping="delib", fileNameProperty="delibName")
     *
     * @var File
     */
    private $delibFile;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $delibName;

    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    private $updatedAt;


    public function getId()
    {
        return $this->id;
    }
    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $delib
     *
     * @return delib
     */
    public function setdelibFile(File $delib = null)
    {
        $this->delibFile = $delib;

        if ($delib) {
            $this->updatedAt = new \DateTimeImmutable();
        }
        return $this;
    }

    /**
     * @return File|null
     */
    public function getdelibFile()
    {
        return $this->delibFile;
    }

    /**
     * @param string $delibName
     *
     * @return delib
     */
    public function setdelibName($delibName)
    {
        $this->delibName = $delibName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getdelibName()
    {
        return $this->delibName;
    }
}

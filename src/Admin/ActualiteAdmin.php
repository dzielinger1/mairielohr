<?php
/**
 * Created by PhpStorm.
 * User: David
 * Date: 20/06/2018
 * Time: 11:36
 */

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ActualiteAdmin extends AbstractAdmin
{

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('titre', TextType::class)
            ->add('description', TextareaType::class)
            ->add('imageFile', FileType::class,[
                'required' => false,
            ])
            ->add('pdfFile', FileType::class,[
                'required' => false,
            ]);;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('titre')
            ->add('description')
            ->add('image')
            ->add('pdf')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('titre')
            ->add('description')
            ->add('image')
            ->add('pdf')
        ;
    }

}
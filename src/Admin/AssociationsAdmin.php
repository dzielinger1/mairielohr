<?php
/**
 * Created by PhpStorm.
 * User: David
 * Date: 10/05/2018
 * Time: 12:45
 */

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class AssociationsAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('nom', TextType::class);
        $formMapper->add('description', TextareaType::class);
        $formMapper->add('adresse', TextType::class,[
            'required' => false,
        ]);
        $formMapper->add('contact', TextType::class,[
            'required' => false,
        ]);
        $formMapper->add('telephone', TextType::class,[
            'required' => false,
        ]);
        $formMapper->add('email', TextType::class,[
            'required' => false,
        ]);
    }
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('nom');
        $listMapper->add('description');
        $listMapper->add('adresse');
        $listMapper->add('contact');
        $listMapper->add('telephone');
        $listMapper->add('email');
    }
}
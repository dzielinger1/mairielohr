<?php
/**
 * Created by PhpStorm.
 * User: David
 * Date: 25/03/2018
 * Time: 13:39
 */

namespace App\Controller;

use App\Entity\Actualite;
use App\Entity\Associations;
use App\Entity\ConseilMunicipal;
use App\Entity\Deliberation;
use App\Entity\Event;
use App\Service\SendEmail;
use App\Entity\Lieux;
use App\Form\ContactType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */

    public function indexAction(Request $request) {

        $repository = $this->getDoctrine()->getRepository(Event::class);

        $dateJour = new \DateTime();

        $events = $repository->findByDate($dateJour);

        return $this->render('views/index.html.twig', ["events" => $events]);
    }

    /**
     * @Route("/mairie", name="mairie")
     */

    public function mairieAction(Request $request) {
        return $this->render('views/mairie.html.twig');
    }

    /**
     * @Route("/conseil", name="conseil")
     */

    public function conseilAction(Request $request) {

        $ConseilRepository = $this->getDoctrine()->getRepository(ConseilMunicipal::class);
        $DelibRepository = $this->getDoctrine()->getRepository(Deliberation::class);

        $conseil = $ConseilRepository->findAll();
        $delibs = $DelibRepository->findBy([],['updatedAt' => 'DESC'],3);

        return $this->render('views/conseil.html.twig', ["conseil" => $conseil,"delibs" => $delibs]);
    }

    /**
     * @Route("/eglise", name="eglise")
     */

    public function egliseAction(Request $request) {
        return $this->render('views/eglise.html.twig');
    }

    /**
     * @Route("/bibliotheque", name="bibliotheque")
     */

    public function biblothequeAction(Request $request) {
        return $this->render('views/biblio.html.twig');
    }

    /**
     * @Route("/ecole", name="ecole")
     */

    public function ecoleAction(Request $request) {
        return $this->render('views/ecole.html.twig');
    }

    /**
     * @Route("/associations", name="associations")
     */

    public function associationsAction(Request $request) {

        $repository = $this->getDoctrine()->getRepository(Associations::class);

        $associations = $repository->findAll();

        return $this->render('views/associations.html.twig', ["associations" => $associations]);
    }

    /**
     * @Route("/location", name="location")
     */

    public function locationAction() {
        return $this->render('views/location.html.twig');
    }

    /**
     * @Route("/village", name="village")
     */

    public function villageAction() {
        return $this->render('views/village.html.twig');
    }

    /**
     * @Route("/architecture", name="architecture")
     */

    public function architectureAction() {
        return $this->render('views/architecture.html.twig');
    }

    /**
     * @Route("/histoire", name="histoire")
     */

    public function histoireAction() {
        return $this->render('views/histoire.html.twig');
    }

    /**
     * @Route("/geographie", name="geographie")
     */

    public function geographieAction() {
        return $this->render('views/geographie.html.twig');
    }

    /**
     * @Route("/lotissement", name="lotissement")
     */

    public function lotissementAction() {
        return $this->render('views/lotissement.html.twig');
    }

    /**
     * @Route("/contact", name="contact")
     * @param Request $request
     * @param SendEmail $sendEmail
     * @return \Symfony\Component\HttpFoundation\Response
     */

    public function contactAction(Request $request) {

        $form = $this->createForm(ContactType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid() && $this->captchaverify($request->get('g-recaptcha-response'))) {

            $data = $form->getData();

            $to      = 'mairie@lohr.alsace';
            $subject = $data['name']  ." ". $data['subject'];
            $message = $data['message'];
            $headers = 'From: webmaster@lohr.alsace' . "\r\n" .
                'Reply-To:' . $data['email'] . "\r\n" ;

            mail($to, $subject, $message, $headers);

            $this->addFlash("success", "Votre message a bien été envoyé");

        }

        return $this->render('views/contact.html.twig',['form'=>$form->createView()]);
    }

    /**
     * @Route("/evenementdetail/{id}", name="evenementdetail")
     */

    public function evenementDetailAction(Request $request, Event $event) {

        return $this->render('views/evenementDetail.html.twig',['event' => $event]);
    }

    /**
     * @Route("/actualite", name="actualite")
     */

    public function actualiteAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository(Actualite::class);

        $actualites = $repository->findAll();

        return $this->render('views/actualite.html.twig',['actualites' => $actualites]);
    }

    /**
     * @Route("/mentionlegal", name="mentionlegal")
     */

    public function mentionLegaleAction(Request $request)
    {

        return $this->render('views/mentionLegal.html.twig');
    }

    function captchaverify($recaptcha){
        $url = "https://www.google.com/recaptcha/api/siteverify";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, array(
            "secret"=>"6LeFpGIUAAAAAFNFWqAM2HSDIyRsKAXZOc7mkNu1","response"=>$recaptcha));
        $response = curl_exec($ch);
        curl_close($ch);
        $data = json_decode($response);

        return $data->success;
    }
}